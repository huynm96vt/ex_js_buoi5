function TinhThue() {
  var name = document.getElementById("name").value;
  var thunhap = document.getElementById("thunhap").value * 1;
  var songuoi = document.getElementById("songuoi").value * 1;
  var ThuNhapChiuThue = thunhap - 4000000 - songuoi * 1600000;
  var tienthue = null;

  if (ThuNhapChiuThue <= 60000000) {
    tienthue = ThuNhapChiuThue * 0.05;
  } else if (ThuNhapChiuThue <= 120000000) {
    tienthue = ThuNhapChiuThue * 0.1;
  } else if (ThuNhapChiuThue <= 210000000) {
    tienthue = ThuNhapChiuThue * 0.15;
  } else if (ThuNhapChiuThue <= 384000000) {
    tienthue = ThuNhapChiuThue * 0.2;
  } else if (ThuNhapChiuThue <= 624000000) {
    tienthue = ThuNhapChiuThue * 0.25;
  } else if (ThuNhapChiuThue <= 960000000) {
    tienthue = ThuNhapChiuThue * 0.3;
  } else {
    tienthue = ThuNhapChiuThue * 0.35;
  }
  document.getElementById(
    "tienthue"
  ).innerHTML = `${name} <br> Tiền thuế: ${tienthue}`;
}
